<?php 

return array(
    'routes' => array(
        'cms-admin' => array(
            'type' => 'segment',
            'options' => array(
                'route'    => '/admin',
                'defaults' => array(
                    'controller' => 'cms.controller.admin',
                    'action'     => 'dashboard',
                ),
            ),
            'may_terminate' => true,
            'child_routes' => array(
                'cms-profile' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/profile[/:action]',
                        'defaults' => array(
                            'controller' => 'cms.admin.profile.settings',
                            'action'     => 'index',
                        ),
                    ),
                ),

                'cms-members' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/members[/:action][/:id]',
                        'defaults' => array(
                            'controller' => 'cms.admin.members',
                            'action'     => 'members',
                        ),
                    ),
                ),
                // 'cms-calendar' => array(
                //     'type'    => 'segment',
                //     'options' => array(
                //         'route'    => '/calendar[/:action]',
                //         'defaults' => array(
                //             'controller' => 'cms.controller.calendar',
                //             'action'     => 'index',
                //         ),
                //     ),
                // ),
                // 'cms-messages' => array(
                //     'type'    => 'segment',
                //     'options' => array(
                //         'route'    => '/messages[/:action]',
                //         'defaults' => array(
                //             'controller' => 'cms.controller.messages',
                //             'action'     => 'index',
                //         ),
                //     ),
                // ),
                // 'cms-notifications' => array(
                //     'type'    => 'segment',
                //     'options' => array(
                //         'route'    => '/notifications[/:action]',
                //         'defaults' => array(
                //             'controller' => 'cms.controller.notifications',
                //             'action'     => 'index',
                //         ),
                //     ),
                // ),
                    
            ),                
        ),

        'cms-login' => array(
            'type' => 'literal',
            'options' => array(
                'route' => '/login',
                'defaults' => array(
                    'controller' => 'cms.controller.login',
                    'action'     => 'login',
                )
            ),
        ),

        'cms-logout' => array(
            'type' => 'literal',
            'options' => array(
                'route' => '/logout',
                'defaults' => array(
                    'controller' => 'cms.controller.login',
                    'action'     => 'logout',
                )
            ),
        )
    )
);



