<?php

return array(
    'invokables' => array(
        'cms.controller.admin'       => 'Cms\Client\Admin\Controller\AdminController',
        'cms.controller.login'       => 'Cms\Client\Admin\Controller\LoginController',
        'cms.admin.profile.settings' => 'Cms\Client\Admin\Controller\ProfileController',
        'cms.admin.members'          => 'Cms\Client\Admin\Controller\MembersController'
    )
);