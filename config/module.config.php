<?php

return array(

    'controllers' => include 'controller.config.php',

    'router' => include 'router.config.php',

    'service_manager' => include 'service.config.php',

    'view_manager' => array(
        'template_map' => array(
            'layout/cms-login-layout'         => __DIR__ . '/../view/layout/cms-login-layout.twig',
            'cms/login/login'                 => __DIR__ . '/../view/cms-admin/login/login.twig',
            'layout/cms-admin-layout'         => __DIR__ . '/../view/layout/cms-admin-layout.twig',
            'cms-admin-layout-topmenu'        => __DIR__ . '/../view/layout/cms-admin-layout-topmenu.twig',
            'cms-admin-layout-sidebar'        => __DIR__ . '/../view/layout/cms-admin-layout-sidebar.twig',
            'cms-admin-layout-sidebar-footer' => __DIR__ . '/../view/layout/cms-admin-layout-sidebar-footer.twig', 
            'cms-admin-layout-chatmenu'       => __DIR__ . '/../view/layout/cms-admin-layout-chatmenu.twig',
            'cms/admin/dashboard'             => __DIR__ . '/../view/cms-admin/admin/dashboard.twig',
            'cms/members/members'             => __DIR__ . '/../view/cms-admin/admin/members/members.twig',
            'cms/members/create'        => __DIR__ . '/../view/cms-admin/admin/members/create.twig',      
            // 'cms/members/edit'          => __DIR__ . '/../view/cms-admin/admin/members/edit.twig',
            
            // 'cms/profile/settings'      => __DIR__ . '/../view/cms-admin/admin/profile/settings.twig',
            // 'cms/profile/index'         => __DIR__ . '/../view/cms-admin/admin/profile/index.twig',
            
            
            // 'cms-admin/users/index'      => __DIR__ . '/../view/cms-admin/admin/users/index.phtml',
            // 'cms-admin/calendar/index'   => __DIR__ . '/../view/cms-admin/admin/calendar/index.phtml',
            // 'cms-admin/messages/index'   => __DIR__ . '/../view/cms-admin/admin/messages/index.phtml',
            // 'cms-admin/notifications/index'   => __DIR__ . '/../view/cms-admin/admin/notifications/index.phtml'
        )
    ),

    // 'module_layouts' => array(
    //     'Cms\Admin'   => 'layout/admin-layout',
    // ),

    // 'bas_cms' => array(
    //     'extensions' => array(
    //         'admin-manager' => array(
    //             'type'    => 'CmsAdmin\Extension\AdminManager',
    //             'options' => array()
    //         )
    //     )
    // )
);