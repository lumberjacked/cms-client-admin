<?php
namespace Cms\Client\Admin\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;

class LoginController extends AbstractActionController {
    
    public function loginAction() {

        if($this->identity()) {
            return $this->redirect()->toRoute('cms-admin');    
        }

        $request = $this->getRequest();

        if($request->isPost()) {
            
            $data     = $request->getPost()->toArray();
            $xmanager = $this->plugin('cms.extension.plugin')->getXmanager();
            
            $data['grant_type'] = 'client_credentials';
            $data['headers'] = array('auth' => array('email' => $data['email'], 'password' => $data['password']));

            $responder = $xmanager->client('login.request', 'post', 'login', $data);
            
            if(!$responder->isError()) {
                return $this->redirect()->toRoute('cms-admin');
            }
        }

    }

    public function logoutAction() {
        
        if($this->identity()) {
            $auth = $this->plugin('cms.extension.plugin')->get('auth-manager');
            $auth->logout();
        }

        return $this->redirect()->toRoute('cms-login');
    }
} 