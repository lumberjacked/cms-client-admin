<?php
namespace Cms\Client\Admin\Controller;

use Zend\Stdlib\Parameters;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;

class MembersController extends AbstractActionController {

    public function membersAction() {
        
     	$xmanager = $this->plugin('cms.extension.plugin')->getXmanager(); 
     	
     	$responder = $xmanager->api('get', 'members');
        
        return array('response' => $responder->toArray());
        
    }

    public function createAction() {
        
        $request = $this->getRequest();
        
        if($request->isPost()) {
           
          $xmanager = $this->plugin('cms.extension.plugin')->getXmanager();
          $data = $request->getPost();
          
          $responder = $xmanager->api('post', 'members', $data->toArray());
          var_dump($responder);die();
          //$data      = new Parameters($responder->data());
          
          return $this->redirect()->toRoute('cms-admin/cms-members', array('action'=>'profile', 'id' => $data->members['id']));
        }
    }

    public function editAction() {
        
        $request = $this->getRequest();

        if($request->isPost()) {
            
            $xmanager = $this->plugin('cms.extension.plugin')->getXmanager();
            $params   = array('id' => $this->params()->fromRoute('id'), 'data' => $request->getPost()->toArray());
            
            $responder = $xmanager->api('put', 'members', $params);
            
            return array('response' => $responder->toArray());

        } else {
            
            $params = array('id' => $this->params()->fromRoute('id'));
             
            $xmanager = $this->plugin('cms.extension.plugin')->getXmanager(); 
            
            $responder = $xmanager->api('get', 'members', $params);
            $data = $responder->toArray();
            
            if($data['error'] !== true) {

                return array('response' => $responder->toArray());

            } else {
                return $this->redirect()->toRoute('cms-admin/cms-members');  
            }
            


        } 
    }

    public function profileAction() {
        var_dump($this->params()->fromRoute('id'));die();   
    }

    public function deleteAction() {
        var_dump($this->params()->fromRoute('id'));die();   
    }

   
} 